#!/bin/bash

CONTROLLER_IP=10.138.0.2
PASSWORD=pful1234!

# for Preparation

source "../../common-rocky/common-rocky.sh"

# Step 2: SQL database
set -ex; 
{
		echo "mariadb-server-10.3" mysql-server/root_password password $PASSWORD;
		echo "mariadb-server-10.3" mysql-server/root_password_again password $PASSWORD;
} | debconf-set-selections;

apt install -y mariadb-server python-pymysql
cp 99-openstack.cnf /etc/mysql/mariadb.conf.d

service mysql restart

# Step 3: Message queue
apt install -y rabbitmq-server
service rabbitmq-server restart
rabbitmqctl add_user openstack $PASSWORD
rabbitmqctl set_permissions openstack ".*" ".*" ".*"

# Step 4: Memcached
apt install -y memcached python-memcache
cp memcached.conf /etc
service memcached restart

# for Keystone

# Step 5: Install and configure
mysql -uroot -p'$PASSWORD' < db.sql

apt install -y keystone apache2 libapache2-mod-wsgi
cp keystone.conf /etc/keystone

/bin/sh -c "keystone-manage db_sync" keystone

keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
keystone-manage credential_setup --keystone-user keystone --keystone-group keystone

keystone-manage bootstrap --bootstrap-password $PASSWORD \
  --bootstrap-admin-url http://$CONTROLLER_IP:5000/v3/ \
  --bootstrap-internal-url http://$CONTROLLER_IP:5000/v3/ \
  --bootstrap-public-url http://$CONTROLLER_IP:5000/v3/ \
  --bootstrap-region-id RegionOne

# Step 6: Configure the Apache HTTP server

cp apache2.conf /etc/apache2
service apache2 restart

export OS_USERNAME=admin
export OS_PASSWORD=$PASSWORD
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://$CONTROLLER_IP:5000/v3
export OS_IDENTITY_API_VERSION=3
