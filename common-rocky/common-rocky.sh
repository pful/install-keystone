apt update

# Step 0

apt install -y vim net-tools

# Step 1: OpenStack packages
apt install -y software-properties-common
echo | add-apt-repository cloud-archive:rocky

apt update
apt dist-upgrade -y

apt install -y python-openstackclient